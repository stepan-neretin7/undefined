package fit.nsu.labs.exceptions;

public class CalcException extends Exception {
    CalcException(String message) {
        super(message);
    }
}
